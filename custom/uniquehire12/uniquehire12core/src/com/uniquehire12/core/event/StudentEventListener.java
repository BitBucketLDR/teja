/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Autowired;

import com.uniquehire12.core.model.StudentRegistrationProcessModel;


/**
 * Listener for customer registration events.
 */
public class StudentEventListener extends AbstractAcceleratorSiteEventListener<StudentEvent>
{
	@Autowired
	private ModelService modelService;
	@Autowired
	private BusinessProcessService businessProcessService;



	@Override
	protected void onSiteEvent(final StudentEvent registerEvent)
	{
		final StudentRegistrationProcessModel storeFrontCustomerProcessModel = (StudentRegistrationProcessModel) getBusinessProcessService()
				.createProcess(
						"studentRegistrationEmailProcess-" + registerEvent.getStudent().getEmail() + "-" + System.currentTimeMillis(),
						"studentRegistrationEmailProcess");
		storeFrontCustomerProcessModel.setSite(registerEvent.getSite());
		storeFrontCustomerProcessModel.setCustomer(registerEvent.getCustomer());
		storeFrontCustomerProcessModel.setLanguage(registerEvent.getLanguage());
		storeFrontCustomerProcessModel.setCurrency(registerEvent.getCurrency());
		storeFrontCustomerProcessModel.setStore(registerEvent.getBaseStore());
		storeFrontCustomerProcessModel.setId(registerEvent.getStudent().getId());
		storeFrontCustomerProcessModel.setName(registerEvent.getStudent().getName());
		storeFrontCustomerProcessModel.setEmail(registerEvent.getStudent().getEmail());
		storeFrontCustomerProcessModel.setPhoneNumber(registerEvent.getStudent().getPhoneNumber());
		getModelService().save(storeFrontCustomerProcessModel);
		getBusinessProcessService().startProcess(storeFrontCustomerProcessModel);
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final StudentEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
