package com.uniquehire12.core.jalo;

import com.uniquehire12.core.constants.Uniquehire12CoreConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class Uniquehire12CoreManager extends GeneratedUniquehire12CoreManager
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger( Uniquehire12CoreManager.class.getName() );
	
	public static final Uniquehire12CoreManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Uniquehire12CoreManager) em.getExtension(Uniquehire12CoreConstants.EXTENSIONNAME);
	}
	
}
