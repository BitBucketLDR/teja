/**
 *
 */
package com.uniquehire12.core.student.dao;

import java.util.List;

import com.uniquehire12.core.model.StudentModel;


/**
 * @author manjunath varadha
 *
 */
public interface StudentDao
{

	List<StudentModel> getAllstudent();

	StudentModel getstudentById(String id);

	void register(StudentModel student);


}
