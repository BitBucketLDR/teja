/**
 *
 */
package com.uniquehire12.core.student.dao;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.uniquehire12.core.model.StudentModel;


/**
 * @author manjunath varadha
 *
 */
public class StudentDaoImpl implements StudentDao
{
	@Autowired
	private FlexibleSearchService flexibleSearchService;
	@Autowired
	private ModelService modelService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.dao.StudentDao#getAllstudent()
	 */
	@Override
	public List<StudentModel> getAllstudent()
	{
		final SearchResult<StudentModel> searchResult = flexibleSearchService.search("select {pk} from {student}");
		return searchResult.getResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.dao.StudentDao#getstudentById(java.lang.String)
	 */
	@Override
	public StudentModel getstudentById(final String id)
	{
		final Map<String, String> param = new HashMap<>();

		param.put("id", id);

		final SearchResult<StudentModel> searchResult = flexibleSearchService.search("select {pk} from {student} where {id} = ?id",
				param);
		return searchResult.getResult().get(0);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.dao.StudentDao#register(com.uniquehire12.core.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{
		modelService.save(student);

	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



}
