/**
 *
 */
package com.uniquehire12.core.student.service;

import java.util.List;

import com.uniquehire12.core.model.StudentModel;


/**
 * @author manjunath varadha
 *
 */
public interface StudentService
{
	List<StudentModel> getAllstudent();

	StudentModel getstudentById(String id);

	void register(StudentModel student);


}
