/**
 *
 */
package com.uniquehire12.core.student.service;

import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.uniquehire12.core.event.StudentEvent;
import com.uniquehire12.core.model.StudentModel;
import com.uniquehire12.core.student.dao.StudentDao;


/**
 * @author manjunath varadha
 *
 */
public class StudentServiceImpl implements StudentService
{

	@Autowired
	private StudentDao studentDao;


	@Autowired
	private BaseStoreService baseStoreService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private EventService eventService;




	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.service.StudentService#getAllstudent()
	 */
	@Override
	public List<StudentModel> getAllstudent()
	{
		// YTODO Auto-generated method stub
		return studentDao.getAllstudent();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.service.StudentService#getstudentById(java.lang.String)
	 */
	@Override
	public StudentModel getstudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return studentDao.getstudentById(id);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.service.StudentService#register(com.uniquehire12.core.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{
		studentDao.register(student);
		final StudentEvent event = new StudentEvent();
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		event.setStudent(student);
		eventService.publishEvent(event);




	}

	/**
	 * @return the studentDao
	 */
	public StudentDao getStudentDao()
	{
		return studentDao;
	}

	/**
	 * @param studentDao
	 *           the studentDao to set
	 */
	public void setStudentDao(final StudentDao studentDao)
	{
		this.studentDao = studentDao;
	}

	/**
	 * @return the commerceCommonI18NService
	 */

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
