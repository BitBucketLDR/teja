/**
 *
 */
package com.uniquehire12.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.ValueResolver;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import com.uniquehire12.core.model.ApparelProductModel;


/**
 * @author manjunath varadha
 *
 */
public class MySearchProcessResolver implements ValueResolver<ProductModel>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.solrfacetsearch.provider.ValueResolver#resolve(de.hybris.platform.solrfacetsearch.indexer.spi.
	 * InputDocument, de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext, java.util.Collection,
	 * de.hybris.platform.core.model.ItemModel)
	 */
	@Override
	public void resolve(final InputDocument doc, final IndexerBatchContext ibc, final Collection<IndexedProperty> list,
			final ProductModel productModel) throws FieldValueProviderException
	{
		// YTODO Auto-generated method stub

	}

	private String getproduct(final ProductModel product)
	{
		if (product instanceof VariantProductModel)
		{
			final ProductModel productModel = ((VariantProductModel) product).getBaseProduct();
			if (productModel instanceof ApparelProductModel && null != ((ApparelProductModel) productModel).getSearchPurpose())
			{
				return ((ApparelProductModel) productModel).getSearchPurpose();
			}
		}
		else if (null != ((ApparelProductModel) product).getSearchPurpose())
		{
			return ((ApparelProductModel) product).getSearchPurpose();

		}

		return null;

	}

}
