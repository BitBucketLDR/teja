/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.facades.constants;

/**
 * Global class for all Uniquehire12Facades constants.
 */
@SuppressWarnings("PMD")
public class Uniquehire12FacadesConstants extends GeneratedUniquehire12FacadesConstants
{
	public static final String EXTENSIONNAME = "uniquehire12facades";

	private Uniquehire12FacadesConstants()
	{
		//empty
	}
}
