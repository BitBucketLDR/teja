/**
 *
 */
package com.uniquehire12.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.uniquehire12.core.model.ApparelStyleVariantProductModel;


/**
 * @author manjunath varadha
 *
 */
public class ModifiedApparelStyleVarient extends ProductBasicPopulator<ProductModel, ProductData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator#populate(de.hybris.platform.
	 * core.model.product.ProductModel, de.hybris.platform.commercefacades.product.data.ProductData)
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		// YTODO Auto-generated method stub
		super.populate(productModel, productData);


		if (productModel instanceof ApparelStyleVariantProductModel)

		{


			final ApparelStyleVariantProductModel model = (ApparelStyleVariantProductModel) productModel;

			productData.setMytext(model.getMytext());




		}
	}

}
