/**
 *
 */
package com.uniquehire12.facades.populators;

import de.hybris.platform.commercefacades.Student.data.StudentData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.uniquehire12.core.model.StudentModel;


/**
 * @author manjunath varadha
 *
 */
public class StudentPopulator implements Populator<StudentModel, StudentData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final StudentModel source, final StudentData target) throws ConversionException
	{
		target.setId(source.getId());
		target.setName(source.getName());
		target.setEmail(source.getEmail());
		target.setPhoneNumber(source.getPhoneNumber());

	}


}
