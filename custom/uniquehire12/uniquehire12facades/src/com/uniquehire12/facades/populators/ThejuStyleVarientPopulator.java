/**
 *
 */
package com.uniquehire12.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.uniquehire12.core.model.ThejuStyleVariantProductModel;


/**
 * @author manjunath varadha
 *
 */
public class ThejuStyleVarientPopulator extends ProductBasicPopulator<ProductModel, ProductData>
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator#populate(de.hybris.platform.
	 * core.model.product.ProductModel, de.hybris.platform.commercefacades.product.data.ProductData)
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		// YTODO Auto-generated method stub
		super.populate(productModel, productData);


		if (productModel instanceof ThejuStyleVariantProductModel)

		{


			final ThejuStyleVariantProductModel model = (ThejuStyleVariantProductModel) productModel;

			productData.setTextField(model.getTextField());




		}
	}

}
