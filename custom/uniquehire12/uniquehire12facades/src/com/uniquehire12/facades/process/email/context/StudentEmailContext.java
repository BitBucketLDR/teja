/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.Student.data.StudentData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.uniquehire12.core.model.StudentRegistrationProcessModel;


/**
 * Velocity context for a customer email.
 */
public class StudentEmailContext extends AbstractEmailContext<StudentRegistrationProcessModel>
{


	StudentData student;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#init(de.hybris.platform.
	 * processengine.model.BusinessProcessModel, de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel)
	 */
	@Override
	public void init(final StudentRegistrationProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		// YTODO Auto-generated method stub
		super.init(businessProcessModel, emailPageModel);
		student = new StudentData();
		student.setId(businessProcessModel.getId());
		student.setName(businessProcessModel.getName());
		student.setEmail(businessProcessModel.getEmail());
		student.setPhoneNumber(businessProcessModel.getPhoneNumber());
		put(DISPLAY_NAME, businessProcessModel.getEmail());
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getSite(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected BaseSiteModel getSite(final StudentRegistrationProcessModel businessProcessModel)
	{
		// YTODO Auto-generated method stub
		return businessProcessModel.getSite();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getCustomer(de.hybris.platform.
	 * processengine.model.BusinessProcessModel)
	 */
	@Override
	protected CustomerModel getCustomer(final StudentRegistrationProcessModel businessProcessModel)
	{
		// YTODO Auto-generated method stub
		final CustomerModel customer = new CustomerModel();
		customer.setCustomerID(businessProcessModel.getId());
		customer.setUid(businessProcessModel.getEmail());
		customer.setName(businessProcessModel.getName());


		return customer;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext#getEmailLanguage(de.hybris.
	 * platform.processengine.model.BusinessProcessModel)
	 */
	@Override
	protected LanguageModel getEmailLanguage(final StudentRegistrationProcessModel businessProcessModel)
	{
		// YTODO Auto-generated method stub
		return businessProcessModel.getLanguage();
	}

}
