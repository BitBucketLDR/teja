/**
 *
 */
package com.uniquehire12.facades.student;

import de.hybris.platform.commercefacades.Student.data.StudentData;

import java.util.List;


/**
 * @author manjunath varadha
 *
 */
public interface StudentFacade
{

	List<StudentData> getAllstudent();

	StudentData getstudentById(String id);

	void register(StudentData student);


}
