/**
 *
 */
package com.uniquehire12.facades.student;

import de.hybris.platform.commercefacades.Student.data.StudentData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.uniquehire12.core.model.StudentModel;
import com.uniquehire12.core.student.service.StudentService;


/**
 * @author manjunath varadha
 *
 */
public class StudentFacadeImpl implements StudentFacade
{
	@Autowired
	private Converter<StudentData, StudentModel> studentRevConverter;
	@Autowired
	private Converter<StudentModel, StudentData> studentConverter;
	@Autowired
	private StudentService studentService;


	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.facades.student.StudentFacade#getAllstudent()
	 */
	@Override
	public List<StudentData> getAllstudent()
	{
		// YTODO Auto-generated method stub
		return studentConverter.convertAll(studentService.getAllstudent());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.facades.student.StudentFacade#getstudentById(java.lang.String)
	 */
	@Override
	public StudentData getstudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return studentConverter.convert(studentService.getstudentById(id));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.facades.student.StudentFacade#register(de.hybris.platform.commercefacades.Student.data.
	 * StudentData)
	 */
	@Override
	public void register(final StudentData student)
	{
		// YTODO Auto-generated method stub
		studentService.register(studentRevConverter.convert(student));



	}

	/**
	 * @return the studentRevConverter
	 */
	public Converter<StudentData, StudentModel> getStudentRevConverter()
	{
		return studentRevConverter;
	}

	/**
	 * @param studentRevConverter
	 *           the studentRevConverter to set
	 */
	public void setStudentRevConverter(final Converter<StudentData, StudentModel> studentRevConverter)
	{
		this.studentRevConverter = studentRevConverter;
	}

	/**
	 * @return the studentConverter
	 */
	public Converter<StudentModel, StudentData> getStudentConverter()
	{
		return studentConverter;
	}

	/**
	 * @param studentConverter
	 *           the studentConverter to set
	 */
	public void setStudentConverter(final Converter<StudentModel, StudentData> studentConverter)
	{
		this.studentConverter = studentConverter;
	}

	/**
	 * @return the studentService
	 */
	public StudentService getStudentService()
	{
		return studentService;
	}

	/**
	 * @param studentService
	 *           the studentService to set
	 */
	public void setStudentService(final StudentService studentService)
	{
		this.studentService = studentService;
	}

	/**
	 * @return the studentService
	 */




}
