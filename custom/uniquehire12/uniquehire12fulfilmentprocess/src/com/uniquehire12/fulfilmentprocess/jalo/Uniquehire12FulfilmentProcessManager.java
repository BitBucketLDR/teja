/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.uniquehire12.fulfilmentprocess.constants.Uniquehire12FulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class Uniquehire12FulfilmentProcessManager extends GeneratedUniquehire12FulfilmentProcessManager
{
	public static final Uniquehire12FulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Uniquehire12FulfilmentProcessManager) em.getExtension(Uniquehire12FulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
