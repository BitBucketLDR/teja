/**
 *
 */
package com.uniquehire12.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.Student.data.StudentData;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.uniquehire12.facades.student.StudentFacade;
import com.uniquehire12.storefront.validator.StudentValidator;


/**
 * @author manjunath varadha
 *
 */

@Controller
@RequestMapping("student")
public class StudentController extends AbstractPageController
{
	@Autowired
	StudentFacade studentFacade;


	@Resource(name = "studentValidator")
	StudentValidator studentValidator;



	/**
	 * @return the studentFacade
	 */
	public StudentFacade getStudentFacade()
	{
		return studentFacade;
	}

	/**
	 * @param studentFacade
	 *           the studentFacade to set
	 */
	public void setStudentFacade(final StudentFacade studentFacade)
	{
		this.studentFacade = studentFacade;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String home(@ModelAttribute("student") final StudentData student, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{


		storeCmsPageInModel(model, getContentPageForLabelOrId("Student"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("Student"));
		updatePageTitle(model, getContentPageForLabelOrId("Student"));

		return getViewForPage(model);
	}

	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}


	@RequestMapping("all")
	public String getAllStudents(final Model model)
	{


		model.addAttribute("students", studentFacade.getAllstudent());
		return "/pages/layout/studentpage";


	}

	@RequestMapping("{id}")
	public String getAllStudents(@PathVariable final String id, final Model model)
	{


		model.addAttribute("student", studentFacade.getstudentById(id));
		return "/pages/layout/studentpage";


	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String getAllStudents(@Valid @ModelAttribute("student") final StudentData student, final Model model,
			final BindingResult result) throws CMSItemNotFoundException
	{

		studentValidator.validate(student, result);

		if (result.hasErrors())
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("Student"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("Student"));
			updatePageTitle(model, getContentPageForLabelOrId("Student"));

			GlobalMessages.addErrorMessage(model, "form.global.error");

			return getViewForPage(model);
		}

		studentFacade.register(student);

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));

		return getViewForPage(model);


	}






}
