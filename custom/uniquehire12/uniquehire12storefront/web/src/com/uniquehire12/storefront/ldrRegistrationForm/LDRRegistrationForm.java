/**
 *
 */
package com.uniquehire12.storefront.ldrRegistrationForm;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


/**
 * @author manjunath varadha
 *
 */
public class LDRRegistrationForm extends RegisterForm
{

	private String phoneNumber;
	private String dateOfBirth;

	/**
	 * @return the dateOfBirth
	 */
	@Override
	public String getDateOfBirth()
	{
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *           the dateOfBirth to set
	 */
	public void setDateOfBirth(final String dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the phoneNumber
	 */
	@Override
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	@Override
	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}



}
