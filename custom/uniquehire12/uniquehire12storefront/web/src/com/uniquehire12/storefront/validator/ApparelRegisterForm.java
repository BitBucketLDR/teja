/**
 *
 */
package com.uniquehire12.storefront.validator;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


/**
 * @author manjunath varadha
 *
 */
public class ApparelRegisterForm extends RegisterForm
{
	private String DateOfBirth;

	/**
	 * @return the dateOfBirth
	 */
	@Override
	public String getDateOfBirth()
	{
		return DateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *           the dateOfBirth to set
	 */
	@Override
	public void setDateOfBirth(final String dateOfBirth)
	{
		DateOfBirth = dateOfBirth;
	}

	/**
	 * @return the phoneNumber
	 */
	@Override
	public String getPhoneNumber()
	{
		return PhoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	@Override
	public void setPhoneNumber(final String phoneNumber)
	{
		PhoneNumber = phoneNumber;
	}

	private String PhoneNumber;


}
