/**
 *
 */
package com.uniquehire12.storefront.validator;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;


/**
 * @author manjunath varadha
 *
 */
public class UniqueRegistrationValidator extends RegistrationValidator
{
	@Override
	public void validate(final Object obect, final Errors errors)
	{
		final ApparelRegisterForm apparelRegisterForm = (ApparelRegisterForm) obect;
		final String titleCode = apparelRegisterForm.getTitleCode();
		final String firstName = apparelRegisterForm.getFirstName();
		final String lastName = apparelRegisterForm.getLastName();
		final String email = apparelRegisterForm.getEmail();
		final String pwd = apparelRegisterForm.getPwd();
		final String checkPwd = apparelRegisterForm.getCheckPwd();
		final String dateOfBirth = apparelRegisterForm.getDateOfBirth();

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);

		validatedateOfBirth(dateOfBirth, errors);
		final Object phoneNumber = null;
		validatephoneNumber(phoneNumber, errors);
	}

	/**
	 * @param phoneNumber
	 * @param errors
	 */
	private void validatephoneNumber(final Object phoneNumber, final Errors errors)
	{
		// YTODO Auto-generated method stub

	}

	/**
	 * @param dateOfBirth
	 * @param errors
	 */
	private void validatedateOfBirth(final String dateOfBirth, final Errors errors)
	{
		// YTODO Auto-generated method stub

	}


}


